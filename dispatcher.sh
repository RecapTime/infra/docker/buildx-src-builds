#!/bin/bash
# shellcheck disable=SC2154

ciPipelineDetails=$CI_PIPELINE_URL
ciJobDetails=$CI_JOB_URL

guildedDispatch() {
    whoToTag="@gitlab-ci-notifications"
    messageInput="$1\n\n$whoToTag"
    embedColor=${embedColor:-"16735232"} # Attempt to match to one of GitLab's brand colors by default
    if [[ "$WEBHOOK_GUILDED_ID" != "2207d848-ded4-40bb-9a39-fb41f00dc9c1" ]] && [[ "$WEBHOOK_GUILDED_TOKEN" != "thisIsWayMoreCursedThanYouThink" ]]; then
        curl --include -X POST \
            --data "{
            \"embeds\": [
                {
                    \"title\": \"$embedTitle\",
                    \"description\": \"$messageInput\",
                    \"url\": \"$ciJobDetails\",
                    \"color\": ${embedColor},
                    \"author\": {
                        \"name\": \"GitLab CI Status\",
                        \"url\": \"$ciPipelineDetails\",
                        \"icon_url\": \"https://gitlab.com/favicon.ico\"
                    }
                }
            ]
            }" \
            "https://media.guilded.gg/webhooks/${WEBHOOK_GUILDED_ID}/${WEBHOOK_GUILDED_TOKEN}"
    else
      echo "error: WEBHOOK_GUILDED_ID and WEBHOOK_GUILDED_TOKEN are required variables to work!"
    fi
}

tgDispatch() {
    humansToTag="@ajhalili2006" # usually Andrei Jiroh since he's also an sysadmin at Recap Time :)
    messageInput="$1\n\n$humansToTag"
    if [[ "$TELEGRAM_BOT_TOKEN" != "12346:placeholderTokenGuild" ]] && [[ "$TELEGRAM_BOT_CHATID" != "todo" ]]; then
        curl --include -X POST \
            --data "{
            \"chat_id\": $TELEGRAM_BOT_CHATID,
            }"
            "https://api.telegram.org/bot$"
    else
      echo "error: TELEGRAM_BOT_TOKEN and TELEGRAM_BOT_CHATID are required variables to work!"
    fi
}