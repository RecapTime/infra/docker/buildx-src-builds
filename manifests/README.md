# Docker Bake Manifests

This repository is where we store all of our own Docker Bake definitions for use in builds.

## Why de-coupling arches?

This is the ensure we don't hit the 3 hour max timeout in GitLab SaaS shared runners and also split the builds into seperate GitLab CI jobs, handled through the `BUILDX_TARGET_ARCH` in the `build_from_source.sh` script.
