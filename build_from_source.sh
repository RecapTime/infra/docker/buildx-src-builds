#!/bin/bash
# shellcheck shell=bash

# We're gonna place the placeholders for handling stuff in shell functions below.
export WEBHOOK_GUILDED_ID=${WEBHOOK_GUILDED_ID:-"2207d848-ded4-40bb-9a39-fb41f00dc9c1"} \
    WEBHOOK_GUILDED_TOKEN=${WEBHOOK_GUILDED_TOKEN:-"thisIsWayMoreCursedThanYouThink"} \
    TELEGRAM_BOT_TOKEN=${TELEGRAM_BOT_TOKEN:-"12346:placeholderTokenGuild"} \
    TELEGRAM_BOT_CHATID=${TELEGRAM_BOT_CHATID:-"todo"} \
    CI_PACKAGE_STEP="build-from-src" \
    BUILDX_TARGET_ARCH=${BUILDX_TARGET_ARCH:-"amd64"} \
    BUILDX_CMD="docker buildx --file ${CI_BUILDS_DIR}/manifests/$BUILDX_TARGET_ARCH.hcl"
ciJobTitle=$CI_JOB_NAME

if [[ "$PWD" == "$(git rev-parse --show-toplevel)" ]]; then
  source "./dispatcher.sh"
else
  source "$(git rev-parse --show-toplevel)/dispatcher.sh"
fi
successful_build() {
    export embedTitle="✔ The \`$ciJobTitle\` CI job successfully ran" embedColor="16711680"
    GUILD_BOT_MESSAGE="Expect newly-built binaries are uploaded to the generic package registry and an new release will be issued."
    TG_BOT_MESSAGE="**Build Success**\n\nThe Buildx CLI plugin source build had successfully built. Expect newly-built binaries are uploaded to the generic package registry and an new release will be issued."

    # Dispatch to different APIs
    guildedDispatch "$GUILD_BOT_MESSAGE"
    tgDispatch "$TG_BOT_MESSAGE"
}

failed_build() {
    export embedTitle="❌ The \`$ciJobTitle\` CI job has been failed." embedColor="16711680"
    GUILD_BOT_MESSAGE="Please check the logs linked in the body title for details on why the build is borked. There's a possibility that the downstream bake manifest file or upstream build scripts are borked, or the GitLab CI configuration is having some issues."
    TG_BOT_MESSAGE="**Build Failure**\n\nThe Buildx CLI plugin source build had been failed. Please check the build logs for details why it's failing."

    # Dispatch to different APIs
    guildedDispatch "$GUILD_BOT_MESSAGE"
    tgDispatch "$TG_BOT_MESSAGE"
}

if [[ "$PWD" != "${CI_BUILDS_DIR}/buildx" ]]; then
  echo "error: Please make sure you have an local copy of docker/buildx repo and you're inside of it.!"
  exit 1
fi

if make release; then
  successful_build "${CI_PACKAGE_STEP}"
fi
